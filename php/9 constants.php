<?php


// define(name, value, case-in/sensitive)

//case-sensitive
define("GREETING", "Welcome to W3Schools.com!");
echo GREETING;


//case-insensitive
define ("TEST", "Welcome to W3Schools.com!", true);
echo test;// no error








//---------CONSTANT ARRAYS-----------

define("cars", ["alfa Romeo", "BMW", "Toyota"]);
echo cars[0];








//--------CONSTANT ARE GLOBAL--------
define ("TEST2", "Welcome to W3Schools.com!");

function test2(){
    echo TEST2;//can access. define automatically global
}

test2();














?>