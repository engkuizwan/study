<html>
    <body>
    <?php

    //------------FILE OPEN-------------
    $myfile = fopen("25.1 tryfile.txt", "r") or die("unable to open file!");
    echo "<h>1. file open</h><br><br>" . fread($myfile,filesize("25.1 tryfile.txt"));
    fclose($myfile);

    /* 
    r = open file for read only
    w = open file for write only(reset and fill new content.will create new file if no exist )
    a = open file for write only(previos content not erased.new file will create if no exist)
    x = creates file for read only. return false if file already exist
    r+ = open a file for read/write. (start at the beginning)
    w+ = open file for read/write. (reset previous content)
    a+ = open file for read/write. (previous content will remain)
    x+ = create new file for read/write. return false if already exist
    
    */


    echo "<br><br><br><br>";


    //-------------------READ FILE-----------------

    $file = fread($myfile,filesize("25.1 tryfile.txt"));


    echo "<br><br><br><br>";



    //------------------CLOSE FILE-----------------

    $myfile = fopen("25.1 tryfile.txt","a+");

    fclose($myfile);



    echo "<br><br><br><br>";



    //-------------------READ SINGLE LINE------------- fgets()

    $myfile = fopen("25.1 tryfile.txt", "a+") or die("unable to open file!");
    echo "<h>4. READ SINGLE LINE</h><br><br>" . fgets($myfile);
    fclose($myfile);




echo "<br><br><br><br>";


    //----------------CHECK IF REACHED LAST LINE----------- feof()
    $myfile = fopen("25.1 tryfile.txt", "a+") or die("unable to open file!");

    echo "<h>5. REACHED LAST LINE</h><br><br>";
    while(!feof($myfile)){

        echo  fgets($myfile) . "<br>";
    }

    fclose($myfile);
    

    echo "<br><br><br><br>";

    //------------------- READ SINGLE CHRACTER ------------- fgetc()

    $myfile = fopen("25.1 tryfile.txt", "a+") or die("unable to open file!");

    echo "<h>6. READ SINGLE CHARACTER</h><br><br>" . fgetc($myfile);

    fclose($myfile);






    ?>
    </body>
</html>

