<!-- SIMPLE XML - READ FROM STRING ( simplexml_load_string() ) -->

<?php 
$myXMLData =
"<?xml version='1.0' encoding='UTF-8'?>
<note>
<to>Tove</to>
<from>Jani</from>
<heading>Reminder</heading>
<body>Don't forget me this weekend!</body>
</note>";

$xml=simplexml_load_string($myXMLData) or die("Error: Cannot create object");
print_r($xml);//output : SimpleXMLElement Object ( [to] => Tove [from] => Jani [heading] => Reminder [body] => Don't forget me this weekend! )

echo "<br><br>"
?>



<!-- SIMPLE XML - READ FROM STRING ( simplexml_load_string() ) -->

<?php
$xml=simplexml_load_file("test.xml") or die("Error: Cannot create object");
print_r($xml);
?>