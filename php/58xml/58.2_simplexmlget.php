<?php

$xml=simplexml_load_file("test.xml") or die("Error: Cannot create object");
echo $xml->to . "<br>";
echo $xml->from . "<br>";
echo $xml->heading . "<br>";
echo $xml->body;

echo "<br><br>";
$xml2=simplexml_load_file("test2.xml") or die("Error: Cannot create object");
foreach($xml2->children() as $books) {
    echo $books->title . ", ";
    echo $books->author . ", ";
    echo $books->year . ", ";
    echo $books->price . "<br>";
  }

  //2 xml file content cannot be combined. here we use 2 different xml file which are test.xml & test2.xml

?>