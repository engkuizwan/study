<?php

$name = "ku";
echo "$name";

echo "<br>";

echo "my name is $name"; //same
echo "<br>";
echo "my name is " .$name; //sama


echo "<br>";

$x = 2;
$y = 2;

echo $x + $y;


echo "<br>";


// ----------------GLOBAL AND LOCAL SCOPE-----------


//------------global
$x = 5;

function test() {

    echo $x; //error. $x global scope. just outside

}

test();

echo $x; //can access. 



//-----------local
function test2() {

    $x = 5;
    echo $x; //can $x inside function
}



//-----------------GLOBAL KEYWORD----------------

$x = 5;
$y = 10;

function test3() {
  global $x, $y; // refer variable outside
  $y = $x + $y;
}

test3();
echo $y; // outputs 15


//-------------STATIC KEYWORD---------------

//value variable takkan reset to default = 0


function test4(){

    static $z = 0;
    echo "$z <br>";
    $z++;
    
}

test4();
test4();
test4();
test4();









?>