<?php


//--------While-------

$x = 0;

while($x <= 100) {
  echo "The number is: $x <br>";
  $x+=10;
}


//---------do while---------

$x = 1;

do {
  echo "The number is: $x <br>";
  $x++;
} while ($x <= 5);



//-----for loop--------

for ($x = 0; $x <= 10; $x++) {
    echo "The number is: $x <br>";
}



//----------foreach--------

$age = array("Peter" => "35", "Ben" => "37", "Joe" => "46");

foreach($age as $name => $val){
    echo "$name = $val<br>";
}


?>