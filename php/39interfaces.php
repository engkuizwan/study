<?php


interface animal {
    public function sound();
}

class Cat implements animal {
    public function sound(){
        echo "Meow";
    }
}

class Tiger implements animal {
    public function sound(){
        echo "Rawrrr";
    }
}

class Bird implements animal {
    public function sound(){
        echo "Chip";
    }
}



$cat = new Cat();
$tiger = new Tiger();
$bird = new Bird();

$animal = array ($cat, $tiger, $bird);

// echo '<pre>';
// print_r($animal);
// echo '</pre>';
// die();

foreach($animal as $x){
    
    $x -> sound();
    echo "<br>";
}





?>