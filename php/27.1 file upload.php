<?php


$targetdir = "uploads/";
$targetfile = $targetdir . basename($_FILES["fileToUpload"]["name"]);

$uploadok = 1;

$imagefiletype = strtolower(pathinfo($targetfile, PATHINFO_EXTENSION));

//check if image is actual or fake

if(isset ($_POST["submit"])){
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);

    if ($check == true){
        echo "file is an image -" . $check["mime"] . ".";
        $uploadok = 1;
    }
    else {
        echo "file is not an image";
        $uploadok = 0;
    }
}

// check if file already exist

if (file_exists($targetfile)){
    echo "sorry file already exist";
    $uploadok = 0;
}


// check file size

if ($_FILES["fileToUpload"]["size"] > 500000){
    echo "sorry your file too large";
    $uploadok = 0;
}


// check uploadok

if ($uploadok == 0)
    echo "sorry your file was not uploaded";
else{

    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $targetfile)){
        echo "the file" . htmlspecialchars(basename($_FILES["fileToUpload"]["name"])) . " has been uploaded.";
    }
    else 
        echo "sorry, there was an error uploading your file";
}







?>