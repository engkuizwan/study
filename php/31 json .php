<?php 

//------------------JSON ENCODE------------------

$age = array("Engku" => 22, "Shariq" => 20, "Nabil" => 20);

echo json_encode($age); // {"Engku":22,"Shariq":20,"Nabil":20}

echo "<br><br><br>";

//------------------JSON DECODE-----------------

$jsonobj = '{"Engku":22,"Shariq":20,"Nabil":20}';

var_dump(json_decode($jsonobj)); // object(stdClass)#1 (3) { ["Engku"]=> int(22) ["Shariq"]=> int(20) ["Nabil"]=> int(20) }

echo "<br><br><br>";


//--------------------JSON DECODE (2ND PARAMETER(TRUE))-------------

$jsonobj = '{"Engku":22,"Shariq":20,"Nabil":20}';

var_dump(json_decode($jsonobj, true)); //array(3) { ["Engku"]=> int(22) ["Shariq"]=> int(20) ["Nabil"]=> int(20) }

echo "<br><br><br>";


//-------------------ACCESSING DECODE VALUES------------------------


$jsonobj = '{"Engku":22,"Shariq":20,"Nabil":20}';
$obj = json_decode($jsonobj);

echo $obj -> Engku . "<br>";
echo $obj -> Shariq . "<br>";
echo $obj -> Nabil . "<br>";

/*
22
20
20
*/ 

echo "<br><br><br>";


//----------------------LOOPING THROUGH VALUES-------------------

$jsonobj = '{"Engku":22,"Shariq":20,"Nabil":20}';
$obj = json_decode($jsonobj);

foreach ($obj as $a => $avalue)
    echo $a . " : " . $avalue . "<br>";

    /*
    Engku : 22
    Shariq : 20
    Nabil : 20
 
    */

echo "<br><br><br>";


?>