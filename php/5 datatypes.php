<?php

//---------String----------

$x = "Hello world!";
$y = 'Hello world!';

echo $x;
echo "<br>";
echo $y;

//--------Integer----------

$x = 5985;

//---------Float----------

$x = 10.365;

//---------Boolean------

$x = true;
$y = false;

//------------array----------

$student = array("ahmad", "ali", "Rahim");


//-------OBJECT---------

class Car {

    public $color;
    public $model;

    public function __construct($color,$model)
    {
        $this->color = $color;
        $this->model = $model;
        
    }

    public function message (){

        return "my car is a" . $this->color . " " . $this->model . "!";
    }



}

$mycar = new Car("black", "bezza");
echo $mycar -> message();

echo "<br>";

$mycar = new Car("white", "axia");
echo $mycar -> message();

//------------null--------------







//--------------Resource----------

?>