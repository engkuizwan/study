<?php 

class students{
    public $name; //can be accessed everywhere
    protected $age; // can be accessed within the class and by class drived from that class
    private $count;// ONLY can be accessed within class


    function __construct($name,$age){// constructor function name always start with __
        $this->name;
        $this->age;
    }

    function __destruct() {
        echo "Name: {$this->name}.";
      }


    function setname($name){
        $this->name = $name;
    }

    function getname(){
        return $this->name;
    }

    function setage($age){
        $this->age = $age;
    }

    function getage(){
        return $this->age;
    }

}

?>