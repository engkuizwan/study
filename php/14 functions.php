<?php


/* 

function test(){
    echo "Hello";
}

test();--call the function


*/


//---Loosely Typed Languaged------

//no Strict
function addnumbers(int $a, int $b){
    return $a + $b;
}

echo addnumbers(5, "5 days")


?>


<?php declare(strict_types=1);

//with Strict

function addNumbers1(int $a, int $b) {
    return $a + $b;
  }
  echo addNumbers1(5, "5 days");//error

  
?>


<?php declare(strict_types=1);




//default argument value

function setHeight(int $minheight = 50){
    echo "the height is : $minheight <br>";
}

setHeight(350);
setHeight();//will use default value = 50







//returning values


function sum(int $x, int $y){
    return $z = $x + $y;
}

echo "5+10 = " . sum(5,10) . "<br>";
echo "3+7 = " . sum(3,7);








?>