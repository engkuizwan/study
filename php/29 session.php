<html>
    <body>
        <?php

        session_start(); // start session

        $_SESSION["name"] = "engku";
        $_SESSION["gender"] = "male";

        echo "session vriable is set <br><br>";

        //--------------GET SESSION -----------
        echo "Name :" . $_SESSION["name"] . "<br>";
        echo "Gender : " . $_SESSION["gender"] . ".<br><br>";

        //------------MODIFY SESSION-----------
        $_SESSION["gender"] = "Female";

        echo "Gender :" . $_SESSION["gender"];

        //------------DESTROY SESSION------------

        session_unset();//remove all session variables
        session_destroy(); //destroy the session


        ?>


    </body>
</html>