<html>
    <head>


    <style>

        table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
        }

        th, td {
            padding: 5px;
        }

    </style>


    </head>

    <body>





    <!-- ---------------------------FILTER LIST------------------------- -->
    <table>
        <tr>

        <td>Filter Name</td>
        <td>Filter ID</td>

        </tr>

        <?php

        foreach(filter_list() as $id => $filter){
            echo "<tr><td>" . $filter . "</td><td>" . filter_id($filter) . "</td></tr>";
        }

        ?>

    </table>


        <?php

        //-------------------------SANITIZE STRING---------------------

        echo "<br><br><br>";

        echo "1. sanitize string <br><br>";
        $str = "<h1>Assalamualaikum</h1>";
        
        $newstr = filter_var($str, FILTER_SANITIZE_STRING);
        echo $newstr;



        //--------------------------VALIDATE INTEGER---------------------

        $x = 0;

        echo "<br><br><br>";
        echo "2. Validate integer <br><br>";

        if (filter_var($x, FILTER_VALIDATE_INT) == TRUE || filter_var($x, FILTER_VALIDATE_INT == 0))
            echo ("Integer is valid");
        else
            echo("Integer is not valid");



        //-------------------VALIDATE IP ADDRESS-----------------------


        echo "<br><br><br>";
        echo "3. Validate ip address <br><br>";

        $ip = "127.0.0.1";

        if(filter_var($ip, FILTER_VALIDATE_IP) == true)
            echo "$ip is a valid IP address";
        else
        echo "ip is not a valid IP Address";


        //----------------SANITIZE AND VALIDATE EMAIL ADDRESS-----------

        
        echo "<br><br><br>";
        echo "4. sanitize and validate email address <br><br>";

        $email = "izwan2010@gmail.com";

        $email = filter_var($email, FILTER_SANITIZE_EMAIL);// remove all illegal characters

        if (filter_var($email, FILTER_VALIDATE_EMAIL) == true)
            echo "Email '" . $email . "' is valid";
        else
            echo "Email is not valid";



        //-----------------SANITIZE AND VALIDATE URL--------------


        echo "<br><br><br>";
        echo "5. sanitize and validate url <br><br>";


        $url = "https://www.w3schools.com";

        $url = filter_var($url, FILTER_SANITIZE_URL); //remove illegal characters

        if(filter_var($url, FILTER_VALIDATE_URL) == true)
            echo "URL '" . $url . "' is valid";
        else
        echo "url is not valid";    


        //-----------------VALIDATE INTEGER WITHIN RANGE--------------


        echo "<br><br><br>";
        echo "6. validate integer within range <br><br>";

        $int = 100;
        $max = 150;
        $min = 1;

        if(filter_var($int, FILTER_VALIDATE_INT, array("options" => array("min_range"=>$min, "max_range"=>$max))) == true)
      //if(filter_var($int, FILTER_VALIDATE_INT, array("options" => array("min_range"=>$min, "max_range"=>$max))) === false)
            echo  "Integer within a range";
        else
            echo "Integer not within a range"

        
        //--------------------


        ?>

        










    </body>
</html>