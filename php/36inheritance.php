



<?php

class fruit{
    public $name;
    public $colour;


    public function __construct($name, $colour)
    {
        $this->name = $name;
        $this->colour = $colour;
    }

    /**
     * @return mixed
     */
    public function intro()
    {
        echo "the fruit is {$this->name} and colour is {$this->colour}";
    }

}

class strawberry extends fruit {
    public function message(){
        echo "am i a fruit berry?";
    }
}

$strawberry = new strawberry("strawberry","red");

$strawberry->message();
echo "<br><br>";
$strawberry->intro();



?>