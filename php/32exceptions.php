<?php


//-------------THROW EXCEPTION----------------
function divide ($dividend, $divisor){

    if($divisor == 0)
        throw new Exception("Division by zero");

    return $dividend / $divisor;
}

//  echo divide(5,0);

 /* 
 Fatal error: Uncaught Exception: Division by zero in C:\laragon\www\notes\php\32 exceptions.php:8 Stack 
 trace: #0 C:\laragon\www\notes\php\32 exceptions.php(13): divide(5, 0) #1 {main} thrown in 
 C:\laragon\www\notes\php\32 exceptions.php on line 8
 
 */


echo "<br><br><br>";

//---------------------TRY..CATCH STATEMENT----------

try{
    echo divide(5,0);
}catch(Exception $e){
    // echo "unable to divide";
}

//unable to divide
echo "<br><br><br>";


//-----------------TRY....CATCH....FINALLY STATEMENT----------

try{
     echo divide(5,0);
}catch(Exception $e){
    echo "unable to divide";
}finally{
    echo "process complete";
}

//unable to divideprocess complete

?>