<?php

$students = array("ahmad", "ali", "engku");


//-----------indexed array---------

$cars = array("Volvo", "BMW", "Toyota");
$arrlength = count($cars);

for($x = 0; $x < $arrlength; $x++) {
  echo $cars[$x];
  echo "<br>";
}


//-----------associative array---------

$age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");

/* or

$age['Peter'] = "35";
$age['Ben'] = "37";
$age['Joe'] = "43";

*/
foreach($age as $x => $x_value) {
  echo "Key=" . $x . ", Value=" . $x_value;
  echo "<br>";
}

//-----------two multidimensional array ----------

for ($row = 0; $row < 4; $row++) {
    echo "<p><b>Row number $row</b></p>";
    echo "<ul>";
    for ($col = 0; $col < 3; $col++) {
      echo "<li>".$cars[$row][$col]."</li>";
    }
    echo "</ul>";
  }

?>