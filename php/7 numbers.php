<?php

//------ PHP Casting String and Floats to Integers---

//Float to int
$x = 23423.456456;
$y = (int)$x;
echo $y; //23423

//String to int
$x = "23423.456456";
$y = (int)$x;
echo $y; //23423




?>