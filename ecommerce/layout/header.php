<?php
define('baseurl', 'http://localhost/notes/ecommerce/');
include '../config/config.php';
$dbcon = condb();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="../css/header.css">
    <link rel="stylesheet" href="../css/custprofile.css">    
    <link href="https://emoji-css.afeld.me/emoji.css" rel="stylesheet">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>

<div class="navbar">

    <a class="icon" href="tenant-homepage.jsp" >E-COMMERCE</a>

    <form >
        <input type="hidden" name="action" value="logout">
        <button style="background-color: transparent; border: none; padding-left: 15px; display: inline-block; position: relative; text-align: center;"type="submit" formaction="">LOGOUT</button>
    </form>

    <a href="../views/custprofile.php">PROFILE</a>
    <a href="" >PAYMENT</a>
    <a href="" >CART</a>
    <a href="" >HOME</a>


</div>

</body>
</html>