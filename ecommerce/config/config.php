<?php



function valinput($data){
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    $data = trim($data);
    return $data;
}


function condb(){

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "phpsys";

    $conn = new PDO("mysql:host=$servername;dbname=phpsys", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
     


    return $conn;
    
}

?>