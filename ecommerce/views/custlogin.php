<htlm>
    <head>
        
        <title>Login.Customer</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/login.css">
        <link href="https://emoji-css.afeld.me/emoji.css" rel="stylesheet">

    </head>
    
    <body>

    <?php
        include '../config/config.php';

        if($_GET['id'] != null){

            $id=valinput($_GET['id']);

        $dbcon = condb();

        $stmt = $dbcon->prepare("SELECT * from user where id=:id");


        $pre['id'] = $id;
        $stmt->execute($pre);

        $data=$stmt->fetchAll(PDO::FETCH_CLASS);

        }

        


    ?>


    <div class="header">
    <p>ECOMMERCE</p>
    </div>
    
    <form action="../controllers/custlogin.php" method="POST">
        <div class="container">

            <h2>CUSTOMER LOGIN</h2>

            <div class="row">
    
                <div class="col-25">
                    <label>Username</label>
                </div>

                <div class="col-75">
                    <input type="text" name="custusername" placeholder="Enter your username" value="<?=$data[0]->username?>">
                </div>
            </div>
            
            <div class="row">
                
                <div class="col-25">
                    <label>Password</label>
                </div>

                <div class="col-75">
                    <input type="password" name="custpassword" placeholder="Your password" value="{{<?=$data[0]->password?>}}">
                </div>

            </div>


            <input type="hidden" name="action" value="login">
    
            <button type="submit" class="button button1" name="submit" >Login</button>
    
            <p>Not registered? <a href="custregister.html" style="color: #0e6d69; text-decoration: none; margin-top: 0%">Create an account</a></p>
    
        </div>
    </form>
    
    
    
</body>
</htlm>