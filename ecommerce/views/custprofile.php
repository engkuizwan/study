<?php
include '../layout/header.php';
session_start();
$id = $_SESSION['id'];

$query = "select * from user 
                where id=:id";
        $stmt = $dbcon->prepare($query);
        $stmt->bindParam(':id', $id, PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

    
?>

<div class="container">
        <h3>MY PROFILE</h3>

        <input type="number" name="landlordid"  value="${result.landlordid}" hidden>

        <div class="row">
            <div class="col-25">
                <label>USERNAME</label>
            </div>
            <div class="col-75">
                <label><?=$result['username']?></label>
            </div>
        </div>

        <div class="row">
            <div class="col-25">
                <label>FULL NAME</label>
            </div>
            <div class="col-75">
                <label><?=$result['fullname']?></label>
            </div>
        </div>

        <div class="row">
            <div class="col-25">
                <label>EMAIL</label>
            </div>
            <div class="col-75">
                <label><?=$result['email']?></label>
            </div>
        </div>


 

        <input type="hidden" name="action"  value="delete">
        
        <form >
            <button type="submit"  class="button button1" name="submit" onclick="form.action='../views/custupdate.php'"  onclick="return confirm('Confirm update your profile?');">UPDATE</button>
        </form>
        
        <form>
            <button type="submit" class="button button1" name="submit" formaction="../controllers/custdelete.php" onclick="return confirm('Confirm delete your profile?');" >DELETE</button>
        </form>

    </div>




<?php
include '../layout/footer.php';
?>
