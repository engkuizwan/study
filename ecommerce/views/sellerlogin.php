<htlm>
    <head>
        
        <title>Login.Seller</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="/ecommerce/css/login.css">
        <link href="https://emoji-css.afeld.me/emoji.css" rel="stylesheet">

    </head>
    
    <body>

    <div class="header">
    <p>ECOMMERCE</p>
    </div>
    
    <form action="../controllers/sellerlogin.php" method="POST">
        <div class="container">

            <h2>SELLER LOGIN</h2>

            <div class="row">
    
                <div class="col-25">
                    <label>Username</label>
                </div>

                <div class="col-75">
                    <input type="text" name="sellerusername" placeholder="Enter your username" value="">
                </div>
            </div>
            
            <div class="row">
                
                <div class="col-25">
                    <label>Password</label>
                </div>

                <div class="col-75">
                    <input type="password" name="sellerpassword" placeholder="Your password" value="">
                </div>

            </div>

    
            <button type="submit" class="button button1" name="submit" >Login</button>
    
            <p>Not registered? <a href="sellerregister.html" style="color: #0e6d69; text-decoration: none; margin-top: 0%">Create an account</a></p>
    
        </div>
    </form>
    
    
    
</body>
</htlm>