<?php
include '../layout/header.php';
session_start();
$id = $_SESSION['id'];

$query = "select * from user 
                where id =:id";
        $stmt = $dbcon->prepare($query);
        $stmt->bindParam(':id', $id, PDO::PARAM_STR);

        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);


?>

<form action="../controllers/custupdate.php" method="POST">
    <div class="container">
        <h2>UPDATE MY ACCOUNT</h2>

    

        <div class="row">
            <div class="col-25">
                <label>USERNAME</label>
            </div>
            <div class="col-75">
                <input type="text" name="custusername" value="<?=$result['username']?>">
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label>Password</label>
            </div>
            <div class="col-75">
                <input type="text" name="custpassword" value="<?=$result['password']?>">
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label>FULL NAME</label>
            </div>
            <div class="col-75">
                <input type="text" name="custfullname" value="<?=$result['fullname']?>">
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label>EMAIL</label>
            </div>
            <div class="col-75">
                <input type="email" name="custemail" value="<?=$result['email']?>">
            </div>
        </div>


    

        <button type="submit" class="button button1" name="submit" >Submit</button><br><br>
    </div>
</form>

<?php
include '../layout/footer.php';
?>
