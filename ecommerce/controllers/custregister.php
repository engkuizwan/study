<?php

include '../config/config.php';

$valid = true;
if($_SERVER['REQUEST_METHOD'] == 'POST'){

    if(empty($_POST['custusername'])){
        $custusernameErr = "Enter you username!";
        $valid = false;
    }else{
        $custusername = valinput($_POST['custusername']);
    }

    if(empty($_POST['custpassword'])){
        $custpasswordErr = "Enter you password!";
        $valid = false;
    }else{
        $custpassword = valinput($_POST['custpassword']);
    }

    if(empty($_POST['custname'])){
        $custnameErr = "Enter you full name!";
        $valid = false;
    }else{
        $custname = valinput($_POST['custname']);
    }

    if(empty($_POST['custemail'])){
        $custemailErr = "Enter your email!";
        $valid = false;
    }else{
        $custemail = valinput($_POST['custemail']);
    }

    if($valid == true){
        $dbcon = condb();

        try{
            
            $sql = "INSERT INTO user (username,password,fullname,email)
                    VALUES (:custusername,:custpassword,:custname,:custemail)";
                    
                    $pre=$dbcon->prepare($sql);

                    $pre->bindParam(':custusername', $custusername);
                    $pre->bindParam(':custpassword', $custpassword);
                    $pre->bindParam(':custname', $custname);
                    $pre->bindParam(':custemail', $custemail);

                    $pre->execute();
                    $id = $dbcon->lastInsertId();

                    session_start(); // start session
                    $_SESSION["custid"] = $id;

                    header('Location:../views/custlogin.php?id='.$id);
                    exit();
                    
                    // echo "new record successfully $id";


        }catch(PDOException $e){

            echo $sql . "<br>" . $e->getMessage();

        }

    }




}


?>