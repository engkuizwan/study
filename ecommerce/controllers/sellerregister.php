<?php

include '../config/config.php';

$valid = true;
if($_SERVER['REQUEST_METHOD'] == 'POST'){

    if(empty($_POST['sellerusername'])){
        $sellerusernameErr = "Enter you username!";
        $valid = false;
    }else{
        $sellerusername = valinput($_POST['sellerusername']);
    }

    if(empty($_POST['sellerpassword'])){
        $sellerpasswordErr = "Enter you password!";
        $valid = false;
    }else{
        $sellerpassword = valinput($_POST['sellerpassword']);
    }

    if(empty($_POST['sellername'])){
        $sellernameErr = "Enter you full name!";
        $valid = false;
    }else{
        $sellername = valinput($_POST['sellername']);
    }

    if(empty($_POST['selleremail'])){
        $selleremail = "Enter your email!";
        $valid = false;
    }else{
        $selleremail = valinput($_POST['selleremail']);
    }

    if($valid == true){
        $dbcon = condb();

        try{
            
            $sql = "INSERT INTO customer (username,password,fullname,email)
                    VALUES (:sellerusername,:sellerpassword,:sellername,:selleremail)";
                    
                    $pre=$dbcon->prepare($sql);

                    $pre->bindParam(':sellerusername', $sellerusername);
                    $pre->bindParam(':sellerpassword', $sellerpassword);
                    $pre->bindParam(':sellername', $sellername);
                    $pre->bindParam(':selleremail', $selleremail);

                    $pre->execute();
                    $id = $dbcon->lastInsertId();

                    session_start(); // start session
                    $_SESSION["custid"] = $id;

                    header('Location:../views/sellerlogin.php?id='.$id);
                    exit();
                    
                    // echo "new record successfully $id";


        }catch(PDOException $e){

            echo $sql . "<br>" . $e->getMessage();

        }

    }




}


?>