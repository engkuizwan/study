<html>
<head>
    <title>RENTAL DETAILS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="home_pagination.css">
    <link href="https://emoji-css.afeld.me/emoji.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>

<div class="container">
    <h3>RENTAL DETAILS</h3>
    <form method="POST" action="../assesment_pagination/home_pagination.php">
    <input type="text" name="num_row">
    <button type="submit">SUBMIT</button>
    </form>

    <?php

    include '../assesment_pagination/database.php';
    $num_row = isset($_POST['num_row']) ? $_POST['num_row'] : 20;

    // if(isset($_GET["page"])){
    //     $page = $_GET["page"];
    // }else{
    //     $page = 1;
    // }

    $page = isset($_GET['page']) ? $_GET['page'] : 1;

    


    $start_from = ($page-1)*$num_row;
    $sql = "select r.rental_id, CAST(r.rental_date as DATE) as rental_date, c.first_name, f.title, ci.city 
            from film f
            join inventory i
            on f.film_id = i.film_id
            join rental r
            on i.inventory_id = r.inventory_id
            join customer c
            on r.customer_id = c.customer_id
            join store s
            on c.store_id = s.store_id
            join address a
            on s.address_id = a.address_id
            join city ci
            on a.city_id = ci.city_id
            limit $start_from,$num_row";
    $result = mysqli_query($conn, $sql);

    ?>

    <table id = "myTable">


        <tr>
            <th>NO.</th>
            <th>DATE</th>
            <th>CUSTOMER</th>
            <th>MOVIE TITLE</th>
            <th>BRANCH</th>    
        </tr>
        
        <?php

            while($row = $result->fetch_assoc()) {

        ?>
        
        <tr>
            <td ><?=$row['rental_id']?></td>
            <td ><?=$row['rental_date']?></td>
            <td ><?=$row['first_name']?></td>
            <td ><?=$row['title']?></td>            
            <td ><?=$row['city']?></td>
        </tr>

        <?php
            }
        ?>

    </table>

        <?php

            $sql = 'select * from rental';
            $result = $conn->query($sql);
            $total = mysqli_num_rows($result);
            $total_page= ceil($total/$num_row);

        ?>


<!-- ////////////////////////////////////////////PAGINATION///////////////////////////////////////// -->
        <center>       
        <div class="pagination">
                
                <?php


                    for($i=1; $i<$total_page; $i++){

                        echo "<a href='../assesment_pagination/home_pagination.php?page=".$i."'> ".$i." </a>";
                        
                    }

                ?>
        </div>
        </center>
</div>
</body>
</html>